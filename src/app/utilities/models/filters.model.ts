export interface DynamicFilters {
  ranges: FilterRanges;
  options: {
    climate: string[];
    terrain: string[];
  };
}

export interface FilterRanges {
  rotation_period_min: number;
  rotation_period_max: number;
  orbital_period_min: number;
  orbital_period_max: number;
  diameter_min: number;
  diameter_max: number;
  gravity_min: number;
  gravity_max: number;
  surface_water_min: number;
  surface_water_max: number;
  population_min: number;
  population_max: number;
}
