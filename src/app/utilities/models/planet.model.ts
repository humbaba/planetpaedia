export interface Planet {
  id: number;
  name: string;
  rotation_period: number | null;
  orbital_period: number | null;
  diameter: number | null;
  climate: string[];
  gravity: number | null;
  terrain: string[];
  surface_water: number | null;
  population: number | null;
  residents: string[];
  films: string[];
}
