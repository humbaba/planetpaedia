import { PlanetRaw } from './planet-raw.model';

export interface PlanetPage {
  count: number;
  next: string | null;
  previous: string | null;
  results: PlanetRaw[];
}
