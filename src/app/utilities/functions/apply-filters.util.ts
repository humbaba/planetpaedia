import { Planet, DynamicFilters } from '../models';

export function applyFilters(planet: Planet, filters: DynamicFilters): boolean {
  if (
    (planet.rotation_period < filters.ranges.rotation_period_min) ||
    (planet.rotation_period > filters.ranges.rotation_period_max) ||
    (planet.orbital_period < filters.ranges.orbital_period_min) ||
    (planet.orbital_period > filters.ranges.orbital_period_max) ||
    (planet.diameter < filters.ranges.diameter_min) ||
    (planet.diameter > filters.ranges.diameter_max) ||
    (planet.gravity < filters.ranges.gravity_min) ||
    (planet.gravity > filters.ranges.gravity_max) ||
    (planet.surface_water < filters.ranges.surface_water_min) ||
    (planet.surface_water > filters.ranges.surface_water_max)
    // (planet.population < filters.ranges.population_min) ||
    // (planet.population > filters.ranges.population_max)
  ) {
    return false;
  }

  return true;
}
