export function extractId(url: string): number {
  const stringId = url.substring(29, url.length - 1);
  return +stringId;
}
