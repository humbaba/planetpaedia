export enum NotFoundText {
  PAGE = 'Could not find the page you requested.',
  PLANET = 'Could not find the desired planet.',
  NOTHING = 'Could not find what you were looking for.',
  FILTERS = 'No planets found within the required criteria.'
}
