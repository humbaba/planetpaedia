import { animate, style, transition, trigger } from '@angular/animations';

export const fadeInOutAnimation =
  trigger('fadeInOut', [
    transition(':enter', [
      style({ opacity: 0 }),
      animate('{{ enterDuration }} ease-out')
    ], { params: { enterDuration: '150ms' } }),
    transition(':leave', [
      animate('{{ leaveDuration }} ease-in', style({ opacity: 0 }))
    ], { params: { leaveDuration: '150ms' } })
  ]);
