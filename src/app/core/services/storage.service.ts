import { Injectable } from '@angular/core';
import { Planet, DynamicFilters, RoutedPage, applyFilters } from '@utilities/*';
import { BehaviorSubject, Observable, combineLatest } from 'rxjs';
import { shareReplay, distinctUntilChanged, map } from 'rxjs/operators';

export interface AppState {
  pages: { [pageNr: number]: number[] };
  planets: { [planetId: number]: Planet };
  lastPage: number;
  availableFilters: DynamicFilters | null;
  selectedFilters: DynamicFilters | null;
  currRouteData: { page: RoutedPage, depth: number };
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private appState: AppState;
  private appStateSubject: BehaviorSubject<AppState>;

  constructor() {
    this.appState = {
      pages: {},
      planets: {},
      lastPage: 0,
      availableFilters: null,
      selectedFilters: null,
      currRouteData: null
    };
    this.appStateSubject = new BehaviorSubject(this.appState);
  }

  private setState(changedState: {[key: string]: any}): void {
    const newState = {
      ...this.appState,
      ...changedState
    };
    this.appState = newState;
    this.appStateSubject.next(newState);
  }

  public addPage(pageNr: number, planets: Planet[], lastPage: number): void {
    const pagePlanets = planets.map(planet => planet.id);
    const planetsMapping = planets.reduce((accum, elem) => {
      return { ...accum, [elem.id]: elem };
    }, {});

    const newPages = {
      ...this.appState.pages,
      [pageNr]: pagePlanets
    };
    const newPlanets = {
      ...this.appState.planets,
      ...planetsMapping
    };
    this.setState({ pages: newPages, planets: newPlanets, lastPage });
  }

  public addSinglePlanet(planetId: number, planet: Planet): void {
    const newPlanets = {
      ...this.appState.planets,
      [planetId]: planet
    };
    this.setState({ planets: newPlanets });
  }

  public updateFilters(availableFilters: DynamicFilters): void {
    this.setState({ availableFilters });
  }

  public selectFilters(selectedFilters: DynamicFilters): void {
    this.setState({ selectedFilters });
  }

  public setRouteData(currRouteData: { page: RoutedPage, depth: number }): void {
    this.setState({ currRouteData });
  }

  get state$(): Observable<AppState> {
    return this.appStateSubject.asObservable().pipe(
      shareReplay(1),
      distinctUntilChanged()
    );
  }

  get allPages$(): Observable<{ [pageNr: number]: number[] }> {
    return this.state$.pipe(
      map(state => state.pages),
      distinctUntilChanged()
    );
  }

  get allPlanets$(): Observable<{ [planetId: number]: Planet }> {
    return this.state$.pipe(
      map(state => state.planets),
      distinctUntilChanged()
    );
  }

  get lastPage$(): Observable<number> {
    return this.state$.pipe(
      map(state => state.lastPage),
      distinctUntilChanged()
    );
  }

  get availableFilters$(): Observable<DynamicFilters | null> {
    return this.state$.pipe(
      map(state => state.availableFilters),
      distinctUntilChanged()
    );
  }

  get selectedFilters$(): Observable<DynamicFilters | null> {
    return this.state$.pipe(
      map(state => state.selectedFilters),
      distinctUntilChanged()
    );
  }

  get planetsFiltered$(): Observable<Planet[]> {
    return combineLatest(
      this.allPlanets$,
      this.selectedFilters$
    ).pipe(
      map(([planets, filters]) => {
        if (!planets || !Object.keys(planets).length) {
          return [];
        }
        const planetsArr = Object.keys(planets)
          .reduce((accum, key) => [...accum, planets[key]], []);

        if (!filters || !Object.keys(filters).length) {
          return planetsArr;
        }

        return planetsArr.filter(planet => applyFilters(planet, filters));
      }),
      distinctUntilChanged()
    );
  }

  get routeData$(): Observable<{ page: RoutedPage, depth: number }> {
    return this.state$.pipe(
      map(state => state.currRouteData),
      distinctUntilChanged()
    );
  }

  page$(pageNr: number): Observable<Planet[] | null> {
    return this.state$.pipe(
      map(state => {
        const foundPage = state.pages[pageNr];
        if (foundPage) {
          return foundPage.map(id => state.planets[id]);
        } else {
          return null;
        }
      }),
      distinctUntilChanged()
    );
  }

  planet$(planetId: number): Observable<Planet | null> {
    return this.state$.pipe(
      map(state => {
        const foundSinglePlanet = state.planets[planetId];
        if (foundSinglePlanet) {
          return foundSinglePlanet;
        } else {
          return null;
        }
      }),
      distinctUntilChanged()
    );
  }
}
