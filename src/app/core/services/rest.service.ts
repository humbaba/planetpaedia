import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '@env/environment';
import { Observable, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(
    private http: HttpClient
  ) { }

  readonly baseHttpHeaders: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  readonly API_URL = environment.apiUrl;

  public get(endpoint: string, queryParams?: HttpParams): Observable<any> {
    return this.http.get(
      `${this.API_URL}${endpoint}`,
      { headers: this.baseHttpHeaders, params: queryParams }
      ).pipe(
        retry(2),
        catchError(error => {
          if (!environment.production) {
            console.error('ENCOUNTERED HTTP ERROR:::', error);
          }
          return of(null);
        })
      );
  }
}
