import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef,
  OnDestroy, Output, EventEmitter
} from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { DynamicFilters, FilterRanges } from '@utilities/*';

interface FormControlRanges {
  rotation_period: number[];
  orbital_period: number[];
  diameter: number[];
  gravity: number[];
  surface_water: number[];
  population: number[];
}

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FiltersComponent implements OnInit, OnChanges, OnDestroy {
  @Input() filters: DynamicFilters;
  @Output() selectFilters = new EventEmitter<DynamicFilters | null>();

  private changeSubj$ = new Subject<void>();
  private unsubscribe$ = new Subject<void>();

  ranges: FormControlRanges;
  options: { climate: string[]; terrain: string[]; };
  computedRanges: FilterRanges;

  constructor(
    private changeDetector: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.changeSubj$.pipe(
      takeUntil(this.unsubscribe$),
      debounceTime(200)
    ).subscribe(() => {
      this.computedRanges = { ...this.extractDisplayRanges() };
      this.changeDetector.detectChanges();
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.filters) {
      this.ranges = {
        rotation_period: [1, 100],
        orbital_period: [1, 100],
        diameter: [1, 100],
        gravity: [1, 100],
        surface_water: [1, 100],
        population: [1, 100]
      };
      this.computedRanges = { ...this.extractDisplayRanges() };
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private extractDisplayRanges(): FilterRanges {
    if (!this.filters || !this.filters.ranges || !Object.keys(this.filters.ranges).length || !Object.keys(this.ranges).length) {
      return null;
    }
    return Object.keys(this.filters.ranges).reduce<FilterRanges>((accum, key) => {
      const name = key.substring(0, key.length - 4);
      const modifier = key.substring(key.length - 3, key.length);
      const diff = this.filters.ranges[`${name}_max`] - this.filters.ranges[`${name}_min`];
      const result = (modifier === 'min') ?
        this.filters.ranges[`${name}_min`] + (this.ranges[name][0] / 100) * diff :
        this.filters.ranges[`${name}_min`] + (this.ranges[name][1] / 100) * diff;

      return {
        ...accum,
        [key]: parseFloat(result.toFixed(2))
      };
    }, {} as FilterRanges );
  }

  onSubmit(): void {
    this.selectFilters.emit({ ranges: this.computedRanges, options: this.options });
  }

  clearFilters(): void {
    this.ranges = {
      rotation_period: [1, 100],
      orbital_period: [1, 100],
      diameter: [1, 100],
      gravity: [1, 100],
      surface_water: [1, 100],
      population: [1, 100]
    };
    this.selectFilters.emit(null);
  }

  onControlChange(): void {
    this.changeSubj$.next();
  }
}
