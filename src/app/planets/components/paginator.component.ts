import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginatorComponent implements OnInit {
  @Input() currPage: number;
  @Input() lastPage: number;
  @Output() navigateToPage = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void { }

  goToPage(pageNr: number): void {
    this.navigateToPage.emit(pageNr);
  }
}
