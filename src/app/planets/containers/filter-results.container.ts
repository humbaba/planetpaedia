import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { NotFoundText, Planet } from '@utilities/*';
import { StorageService } from '@core/*';
import { shareReplay, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-filter-results-container',
  templateUrl: './filter-results.container.html',
  styleUrls: ['./filter-results.container.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterResultsContainer implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  planets$: Observable<Planet[]> = this.storage.planetsFiltered$.pipe(shareReplay());
  notFound: boolean;
  notFoundTexts = NotFoundText;

  constructor(
    private storage: StorageService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.planets$.pipe(
      takeUntil(this.unsubscribe$),
      tap(res => (!res || !res.length) && (this.notFound = true))
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public navigateBack(): void {
    this.router.navigate([ `/planets/page/1` ]);
  }

}
