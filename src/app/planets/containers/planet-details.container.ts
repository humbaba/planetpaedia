import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { mergeMap, tap, map, shareReplay, takeUntil } from 'rxjs/operators';
import { NotFoundText, Planet } from '@utilities/*';
import { PlanetService } from '../services';

@Component({
  selector: 'app-planet-details-container',
  templateUrl: './planet-details.container.html',
  styleUrls: ['./planet-details.container.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanetDetailsContainer implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  planet$: Observable<Planet>;
  notFound: boolean;
  currPlanet: number;
  notFoundTexts = NotFoundText;

  constructor(
    private planetService: PlanetService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.planet$ = this.route.paramMap.pipe(
      takeUntil(this.unsubscribe$),
      map((params: ParamMap) => +params.get('planetId')),
      tap(planetId => this.currPlanet = planetId),
      mergeMap(planetId => this.planetService.getSingle(planetId)),
      shareReplay()
    );

    this.planet$.pipe(
      tap(res => !res && (this.notFound = true))
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public prevPlanet(): void {
    this.router.navigate([ `/planet/${this.currPlanet - 1}` ]);
  }

  public nextPlanet(): void {
    this.router.navigate([ `/planet/${this.currPlanet + 1}` ]);
  }

  public navigateBack(): void {
    this.router.navigate([ `/planets/page/1` ]);
  }
}
