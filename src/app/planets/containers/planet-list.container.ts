import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { map, mergeMap, shareReplay, takeUntil, tap } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { PlanetService } from '../services';
import { NotFoundText, Planet } from '@utilities/*';
import { StorageService } from '@core/*';

@Component({
  selector: 'app-planet-list-container',
  templateUrl: './planet-list.container.html',
  styleUrls: ['./planet-list.container.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanetListContainer implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  planets$: Observable<Planet[]>;
  lastPage$: Observable<number> = this.storage.lastPage$;
  currPage: number;
  notFound: boolean;
  notFoundTexts = NotFoundText;

  constructor(
    private planetService: PlanetService,
    private storage: StorageService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.planets$ = this.route.paramMap.pipe(
      takeUntil(this.unsubscribe$),
      map((params: ParamMap) => +params.get('pageNr')),
      tap(pageNr => this.currPage = pageNr),
      mergeMap(pageNr => this.planetService.getByPage(pageNr)),
      shareReplay()
    );

    this.planets$.pipe(
      tap(res => (!res || !res.length) && (this.notFound = true))
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public navigateToPage(pageNr: number): void {
    this.router.navigate([ `/planets/page/${pageNr}` ]);
  }
}
