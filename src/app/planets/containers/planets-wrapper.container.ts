import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '@core/*';
import { DynamicFilters, RoutedPage } from '@utilities/*';

@Component({
  selector: 'app-planets-wrapper',
  templateUrl: './planets-wrapper.container.html',
  styleUrls: ['./planets-wrapper.container.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanetsWrapperContainer implements OnInit {
  routeData$: Observable<{ page: RoutedPage, depth: number }> = this.storage.routeData$;
  filters$: Observable<DynamicFilters> = this.storage.availableFilters$;
  page = RoutedPage;

  constructor(
    private storage: StorageService,
    private router: Router
  ) { }

  ngOnInit(): void { }

  public selectFilters(filters: DynamicFilters | null): void {
    this.storage.selectFilters(filters);
    if (filters) {
      this.router.navigate([ `/planets/filtered` ]);
    }
  }

  public extractDepthIndex(outlet: RouterOutlet): number {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['depthIndex'];
  }

}
