import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetsWrapperContainer, PlanetListContainer, PlanetDetailsContainer, FilterResultsContainer } from './containers';
import { RoutedPage } from '@utilities/*';

const routes: Routes = [
  {
    path: '',
    outlet: 'primary',
    component: PlanetsWrapperContainer,
    children: [
      {
        path: 'planets/page/:pageNr',
        component: PlanetListContainer,
        data: { page: RoutedPage.PLANET_LIST, depth: 1 }
      },
      {
        path: 'planet/:planetId',
        component: PlanetDetailsContainer,
        data: { page: RoutedPage.PLANET_DETAILS, depth: 2 }
      },
      {
        path: 'planets/filtered',
        component: FilterResultsContainer,
        data: { page: RoutedPage.PLANET_FILTERED, depth: 1 }
      },
      {
        path: '',
        redirectTo: 'planets/page/1',
        pathMatch: 'full'
      },
      {
        path: 'planets',
        redirectTo: 'planets/page/1',
        pathMatch: 'full'
      },
      {
        path: 'planets/page',
        redirectTo: 'planets/page/1',
        pathMatch: 'full'
      },
      {
        path: 'planet',
        redirectTo: 'planet/1',
        pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class PlanetsRoutingModule { }
