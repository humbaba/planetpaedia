import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/*';
import { PlanetsRoutingModule } from './planets-routing.module';
import { PlanetsWrapperContainer, PlanetListContainer, PlanetDetailsContainer, FilterResultsContainer } from './containers';
import { PlanetListComponent, PlanetDetailsComponent, PaginatorComponent, FiltersComponent } from './components';
import { PlanetService, FiltersService } from './services';

@NgModule({
  declarations: [
    PlanetsWrapperContainer,
    PlanetListContainer,
    PlanetDetailsContainer,
    FilterResultsContainer,
    PlanetListComponent,
    PlanetDetailsComponent,
    PaginatorComponent,
    FiltersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PlanetsRoutingModule
  ],
  providers: [
    PlanetService,
    FiltersService
  ]
})
export class PlanetsModule { }
