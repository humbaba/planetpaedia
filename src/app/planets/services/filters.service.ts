import { Injectable } from '@angular/core';
import { StorageService } from '@core/*';
import { DynamicFilters, Planet } from '@utilities/*';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  private filters: DynamicFilters;

  constructor(
    private storage: StorageService
  ) {
    this.storage.availableFilters$.subscribe(filters => this.filters = filters);
  }

  private initFilter(planet: Planet): DynamicFilters {
    return {
      ranges: {
        rotation_period_min: planet.rotation_period,
        rotation_period_max: planet.rotation_period,
        orbital_period_min: planet.orbital_period,
        orbital_period_max: planet.orbital_period,
        diameter_min: planet.diameter,
        diameter_max: planet.diameter,
        gravity_min: planet.gravity,
        gravity_max: planet.gravity,
        surface_water_min: planet.surface_water,
        surface_water_max: planet.surface_water,
        population_min: planet.population,
        population_max: planet.population,
      },
      options: {
        climate: planet.climate,
        terrain: planet.terrain
      }
    };
  }

  private updateFilter(planet: Planet, oldFilter: DynamicFilters): DynamicFilters {
    return {
      ranges: {
        rotation_period_min: (planet.rotation_period !== null && planet.rotation_period < oldFilter.ranges.rotation_period_min) ? planet.rotation_period : oldFilter.ranges.rotation_period_min,
        rotation_period_max: (planet.rotation_period > oldFilter.ranges.rotation_period_max) ? planet.rotation_period : oldFilter.ranges.rotation_period_max,
        orbital_period_min: (planet.orbital_period !== null && planet.orbital_period < oldFilter.ranges.orbital_period_min) ? planet.orbital_period : oldFilter.ranges.orbital_period_min,
        orbital_period_max: (planet.orbital_period > oldFilter.ranges.orbital_period_max) ? planet.orbital_period : oldFilter.ranges.orbital_period_max,
        diameter_min: (planet.diameter !== null && planet.diameter < oldFilter.ranges.diameter_min) ? planet.diameter : oldFilter.ranges.diameter_min,
        diameter_max: (planet.diameter > oldFilter.ranges.diameter_max) ? planet.diameter : oldFilter.ranges.diameter_max,
        gravity_min: (planet.gravity !== null && planet.gravity < oldFilter.ranges.gravity_min) ? planet.gravity : oldFilter.ranges.gravity_min,
        gravity_max: (planet.gravity > oldFilter.ranges.gravity_max) ? planet.gravity : oldFilter.ranges.gravity_max,
        surface_water_min: (planet.surface_water !== null && planet.surface_water < oldFilter.ranges.surface_water_min) ? planet.surface_water : oldFilter.ranges.surface_water_min,
        surface_water_max: (planet.surface_water > oldFilter.ranges.surface_water_max) ? planet.surface_water : oldFilter.ranges.surface_water_max,
        population_min: (planet.population !== null && planet.population < oldFilter.ranges.population_min) ? planet.population : oldFilter.ranges.population_min,
        population_max: (planet.population > oldFilter.ranges.population_max) ? planet.population : oldFilter.ranges.population_max,
      },
      options: {
        climate: Object.values([...planet.climate, ...oldFilter.options.climate].reduce((accum, elem) => {
          return { ...accum, [elem]: elem };
        }, {})),
        terrain: Object.values([...planet.terrain, ...oldFilter.options.terrain].reduce((accum, elem) => {
          return { ...accum, [elem]: elem };
        }, {}))
      }
    };
  }

  public extract(...planets: Planet[]): void {
    let newFilters: DynamicFilters = { ...this.filters };
    planets.forEach(planet => {
      if (!Object.keys(newFilters).length) {
        newFilters = this.initFilter(planet);
      } else {
        newFilters = this.updateFilter(planet, newFilters);
      }
    });

    this.storage.updateFilters(newFilters);
  }
}
