import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { mergeMap, tap, take, map, filter } from 'rxjs/operators';
import { RestService, StorageService } from '@core/*';
import {PlanetRaw, Planet, extractId} from '@utilities/*';
import { FiltersService } from './filters.service';

@Injectable({
  providedIn: 'root'
})
export class PlanetService {

  constructor(
    private rest: RestService,
    private storage: StorageService,
    private filters: FiltersService
  ) { }

  readonly PLANET_URL = '/planets';

  private processPlanets(...planets: PlanetRaw[]): Planet[] {
    return planets.map(planet => ({
      id: extractId(planet.url),
      name: planet.name,
      rotation_period: (planet.rotation_period === 'unknown' || planet.rotation_period === 'n/a') ? null : +planet.rotation_period,
      orbital_period: (planet.orbital_period === 'unknown' || planet.orbital_period === 'n/a') ? null : +planet.orbital_period,
      diameter: (planet.diameter === 'unknown' || planet.diameter === 'n/a') ? null : +planet.diameter,
      climate: (planet.climate === 'unknown' || planet.climate === 'n/a') ? [] : planet.climate.split(',').map(str => str.trim()),
      gravity: (planet.climate === 'unknown' || planet.climate === 'n/a') ? null : +planet.gravity.split(' ')[0],
      terrain: (planet.terrain === 'unknown' || planet.terrain === 'n/a') ? [] : planet.terrain.split(',').map(str => str.trim()),
      surface_water: (planet.surface_water === 'unknown' || planet.surface_water === 'n/a') ? null : +planet.surface_water,
      population: (planet.population === 'unknown' || planet.population === 'n/a') ? null : +planet.population,
      residents: planet.residents,
      films: planet.films
    }));
  }

  public getByPage(pageNr: number): Observable<Planet[] | null> {
    return this.storage.page$(pageNr).pipe(
      take(1),
      mergeMap(storedPage => {
        return storedPage ?
          of(storedPage) :
          this.rest.get(`${this.PLANET_URL}?page=${pageNr}`)
            .pipe(
              map(res => {
                if (res) {
                  const planets: Planet[] = this.processPlanets(...res.results);
                  const lastPage = Math.ceil(res.count / 10);

                  this.storage.addPage(pageNr, planets, lastPage);
                  this.filters.extract(...planets);

                  return planets;
                } else {
                  return null;
                }
              })
            );
      })
    );
  }

  public getSingle(planetId: number): Observable<Planet | null> {
    return this.storage.planet$(planetId).pipe(
      take(1),
      mergeMap(storedPlanet => {
        return storedPlanet ?
          of(storedPlanet) :
          this.rest.get(`${this.PLANET_URL}/${planetId}`).pipe(
            map(res => {
              if (res) {
                const planet: Planet = this.processPlanets(res)[0];

                this.storage.addSinglePlanet(planetId, planet);
                this.filters.extract(planet);

                return planet;
              } else {
                return null;
              }
            })
          );
      })
    );
  }
}
