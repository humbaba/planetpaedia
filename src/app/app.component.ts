import { Component, OnInit } from '@angular/core';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { ActivatedRoute, NavigationEnd, RouteConfigLoadEnd, Router } from '@angular/router';
import { StorageService } from './core';
import { RoutedPage } from './utilities';

@Component({
  selector: 'app-root',
  template: `
      <router-outlet name="primary"></router-outlet>
  `,
  styles: [],
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private storage: StorageService
  ) { }

  ngOnInit(): void {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd || event instanceof RouteConfigLoadEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      distinctUntilChanged()
    ).subscribe(route => this.storage.setRouteData(route.snapshot.data as { page: RoutedPage, depth: number }));
  }
}
