import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objKeys'
})
export class ObjKeysPipe implements PipeTransform {
  transform(obj: {[k: string]: any}): string[] {
    if (obj instanceof Array || !(obj instanceof Object)) {
      return [];
    } else {
      return Object.keys(obj);
    }
  }
}
