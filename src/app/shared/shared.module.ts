import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { SliderModule } from 'primeng/slider';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { NotFoundComponent } from './components';
import { ObjKeysPipe } from './pipes';

@NgModule({
  declarations: [
    NotFoundComponent,
    ObjKeysPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SliderModule,
    ButtonModule,
    CardModule
  ],
  exports: [
    FormsModule,
    NotFoundComponent,
    ObjKeysPipe,
    SliderModule,
    ButtonModule,
    CardModule
  ]
})
export class SharedModule { }
